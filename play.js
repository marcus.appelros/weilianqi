const CircleItem = {
	props: {
		info: Object,
		rad: Number
	},
	template: `<circle :cx=info.cx :cy=info.cy :r=rad stroke="purple" :fill=info.color opacity="0.5"><title>{{info.x}},{{info.y}}</title></circle>`
}


const WLQBoard = {
	components: {
		CircleItem
	},
	data(){
		return {
			circles: null,
			height: 500,
			connections: [[0, 1], [1, 0], [1, -1], [0, -1], [-1, 0], [-1, 1]],
			map: [[0,0]],
			size: 5,
			radius: 1,
			origin: [500, 433],
			player: 1,
			history: "",
			colorlock: false,
			board: { map: null, units: [] },
			scoretxt: "",
			newHistory: ""
		}
	},
	methods: {
		onClick(x,y){
			this.place(x,y);
		},
		place(x,y){
			let key=x+","+y;
			let c=this.circles.get(key);
			if (c==undefined){
				this.pass();
				return;
			}
			if (c.player!=0){
				return;
			}
			this.scoretxt="";
			c.color=["red","green","blue"][this.player-1];
			c.player=this.player;
			this.board.units.push([x,y,this.player]);
			this.board.map.set(key,this.player);
			this.history+=key+";";
			this.player+=1;
			if (this.player>3){
				this.player=1;
			}
			if (this.colorlock){
				this.pass();
				this.pass();
			}
		},
		fillMap(layers){
			this.map=[[0, 0]]
			for (let i = 0; i < layers; i++){
				let tgrid = [];
				for (let j=0; j<this.map.length; j++){
					let l=this.map[j];
					for (let k=0; k<this.connections.length; k++){
						let c=this.connections[k];
						let nl = [l[0], l[1]];
						nl[0] += c[0];
						nl[1] += c[1];
						let d=Math.abs(nl[0])+Math.abs(nl[1])+Math.abs(nl[0]+nl[1]);
						if (d==(i+1)*2){
							let present=false;
							for (let l=0; l<tgrid.length; l++){
								if (nl[0]==tgrid[l][0] && nl[1]==tgrid[l][1]){
									present=true;
									break;
								}
							}
							if (!present){
								tgrid.push(nl);
							}
						}
					}
				}
				for (let j=0; j<tgrid.length; j++){
					let loc=tgrid[j];
					this.map.push(loc);
				}
			}
		},
		fillCircles(){
			this.circles=new Map();
			for (let i=0; i<this.map.length; i++){
				x=this.map[i][0];
				y=this.map[i][1];
				xp=this.origin[0]+this.radius*Math.sqrt(3)*(x+y/2);
				yp=this.origin[1]+this.radius*3*y/2;
				this.circles.set(x+","+y,{x: x, y: y, cx: xp, cy: yp, color: "white", player: 0 });
			}
		},
		newBoard(){
			if (this.size<1){
				this.size=1;
			}
			if (this.size>30){
				this.size=30;
			}
			this.player=1;
			this.units=[];
			this.history=this.size+";";
			this.radius=1000/this.size/3.5;
			this.fillMap(this.size-1);
			this.fillCircles();
			this.board=this.makeBoard();
		},
		pass(){
			this.history+="999,0;";
			this.player+=1;
			if (this.player>3){
				this.player=1;
			}
		},
		adjacent(x,y){
			let adj=[];
			for (let i=0; i<this.connections.length; i++){
				let c=this.connections[i];
				tl=[Number(x)+c[0],Number(y)+c[1]];
				if (Math.abs(tl[0])+Math.abs(tl[1])+Math.abs(tl[0]+tl[1])<this.size*2){
					adj.push(tl);
				}
			}
			return adj;
		},
		adjacentLocs(area){
			let adj=[];
			for (let i=0; i<area.length; i++){
				let xy=area[i].split(",");
				let a=this.adjacent(xy[0],xy[1]);
				for (let j=0; j<a.length; j++){
					if (!area.includes(String(a[j]))){
						adj.push(a[j]);
					}
				}
			}
			return adj;
		},
		getArea(b,x,y){
			let area=[];
			let key=x+","+y;
			area.push(key);
			let p=b.map.get(key);
			let c=this.adjacent(x,y);
			while (c.length>0){
				let tc=[];
				for (let i=0; i<c.length; i++){
					let tl=c[i];
					let tkey=String(tl);
					let tkeyc=b.map.get(tkey);
					if (tkeyc==p){
						if (!area.includes(tkey)){
							area.push(tkey);
							let tadj=this.adjacent(tl[0],tl[1]);
							for (let j=0; j<tadj.length; j++){
								tc.push(tadj[j]);
							}
						}
					}
				}
				c=tc;
			}
			return area;
		},
		countAreas(b){
			let as=[0,0,0];
			if (b.units.length==0){
				return as;
			}
			let seg=this.segment(b);
			for (let i=0; i<seg.groups.length; i++){
				let g=seg.groups[i];
				as[g.p-1]+=g.locs.length;
			}
			for (let i=0; i<seg.spaces.length; i++){
				let s=seg.spaces[i];
				let adj=this.adjacentLocs(s);
				let p=b.map.get(String(adj[0]));
				let mono=true;
				for (let j=1; j<adj.length; j++){
					if (b.map.get(String(adj[j]))!=p){
						mono=false;
						break;
					}
				}
				if (mono){
					as[p-1]+=s.length;
				}
			}
			return as;
		},
		countBonds(b){
			let bs=[0,0,0];
			for (let i=0; i<b.units.length; i++){
				let unit=b.units[i];
				let adj=this.adjacent(unit[0],unit[1]);
				for (let j=0; j<adj.length; j++){
					let ad=adj[j];
					let adp=b.map.get(String(ad));
					if (adp>0 && adp!=unit[2]){
						bs[unit[2]-1]+=1;
					}
				}
			}
			return bs;
		},
		makeBoard(){
			let b={ map: new Map(), units: [] };
			for (const key of this.circles.keys()){
				b.map.set(key,0);
			}
			return b;
		},
		segment(b){
			let seg={groups:[], spaces:[]};
			let checked=[];
			for (const [key, p] of b.map.entries()){
				if (!checked.includes(key)){
					let xy=key.split(",");
					let a=this.getArea(b,xy[0],xy[1]);
					if (p==0){
						seg.spaces.push(a);
					} else {
						seg.groups.push({locs:a, p: p, life:0});
					}
					for (let i=0; i<a.length; i++){
						checked.push(a[i]);
					}
				}
			}
			return seg;
		},
		placeP(b,x,y,p){
			b.map.set(x+","+y,p);
			b.units.push([x,y,p]);
		},
		placeArea(b,area,p){
			for (let i=0; i<area.length; i++){
				let xy=area[i].split(",");
				this.placeP(b,xy[0],xy[1],p);
			}
		},
		setLife(seg){
			for (let i=0; i<seg.groups.length; i++){
				let tb=this.makeBoard();
				this.placeArea(tb,seg.groups[i].locs,1)
				let tseg=this.segment(tb);
				if (tseg.spaces.length>1){
					seg.groups[i].life=1;
				}
			}
		},
		score(b){
			let seg=this.segment(b);
			this.setLife(seg);
			let tboard=this.makeBoard();
			for (let i=0; i<seg.groups.length; i++){
				let g=seg.groups[i];
				if (g.life>0){
					this.placeArea(tboard,g.locs,g.p);
				}
			}
			let as=this.countAreas(tboard);
			let bs=this.countBonds(tboard);
			let t=as[0]*as[1]*as[2]*bs[0]*bs[1]*bs[2];
			return [as,bs,t];
		},
		printScore(){
			let s=this.score(this.board);
			this.scoretxt="Territory: red "+s[0][0]+", green "+s[0][1]+", blue "+s[0][2]+"; Bonds: red "+s[1][0]+", green "+s[1][1]+", blue "+s[1][2]+"; Total: "+s[2];
		},
		loadHistory(){
			if (this.newHistory==""){
				alert("Paste a history first");
				return;
			}
			let cl=this.colorlock;
			this.colorlock=false;
			let a=this.newHistory.split(";");
			this.size=Number(a[0]);
			this.newBoard();
			for (let i=1; i<a.length-1; i++){
				let xy=a[i].split(",");
				this.place(xy[0],xy[1]);
			}
			this.colorlock=cl;
		},
		undo(){
			let a=this.history.split(";");
			if (a.length<3){
				return;
			}
			let nh=a[0];
			let offset=2;
			if (this.colorlock){
				offset+=2;
			}
			for (let i=1; i<a.length-offset; i++){
				nh+=";"+a[i];
			}
			nh+=";"
			let oh=this.newHistory;
			this.newHistory=nh;
			this.loadHistory();
			this.newHistory=oh;
		}
	}
}

vm = Vue.createApp(WLQBoard).mount('#wlq-board');
vm.newBoard();
