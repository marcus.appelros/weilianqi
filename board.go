package main

import (
	"errors"
	"strconv"
	"strings"
)

type Loc struct {
	X, Y int
}
type Unit struct {
	Loc Loc
	P   int //player
}
type Board struct {
	Map    map[Loc]int
	Units  []Unit
	Layers int
	Pi     int
	Ps     []int
}

func Makeboard(layers int) Board {
	b := Board{make(map[Loc]int), []Unit{}, layers, 0, []int{1, 2, 3}}
	b.Map[Loc{0, 0}] = 0
	connections := [6]Loc{{1, 0}, {-1, 0}, {0, 1}, {0, -1}, {1, -1}, {-1, 1}}
	for i := 0; i < layers; i++ {
		var tgrid []Loc
		for l, _ := range b.Map {
			for _, c := range connections {
				nl := l
				nl.X += c.X
				nl.Y += c.Y
				tgrid = append(tgrid, nl)
			}
		}
		for _, loc := range tgrid {
			b.Map[loc] = 0
		}
	}
	return b
}
func (b *Board) Reset() {
	b.Units = []Unit{}
	b.Pi = 0
	for l := range b.Map {
		b.Map[l] = 0
	}
}
func (b *Board) PlaceP(l Loc, p int) error {
	if val, ok := b.Map[l]; ok {
		if val > 0 {
			return errors.New("Occupied.")
		}
	} else {
		return errors.New("Outside map.")
	}
	b.Map[l] = p
	b.Units = append(b.Units, Unit{l, p})
	return nil
}
func (b *Board) Place(x, y int) error {
	err := b.PlaceP(Loc{x, y}, b.Ps[b.Pi])
	if err != nil {
		return err
	}
	b.Pi += 1
	if b.Pi >= len(b.Ps) {
		b.Pi = 0
	}
	return nil
}
func (b *Board) Pass() {
	b.Pi += 1
	if b.Pi >= len(b.Ps) {
		b.Pi = 0
	}
}
func (b *Board) Adjacent(l Loc) []Loc {
	connections := [6]Loc{{1, 0}, {-1, 0}, {0, 1}, {0, -1}, {1, -1}, {-1, 1}}
	adj := []Loc{}
	for _, con := range connections {
		tl := l
		tl.X += con.X
		tl.Y += con.Y
		if _, ok := b.Map[tl]; ok {
			adj = append(adj, tl)
		}
	}
	return adj
}
func (b *Board) GetArea(l Loc) map[Loc]struct{} {
	var area = make(map[Loc]struct{})
	area[l] = struct{}{}
	p := b.Map[l]
	c := b.Adjacent(l)
	for len(c) > 0 {
		var tc []Loc
		for _, tl := range c {
			if b.Map[tl] == p {
				_, ok := area[tl]
				if !ok {
					area[tl] = struct{}{}
					tadj := b.Adjacent(tl)
					for _, ttloc := range tadj {
						tc = append(tc, ttloc)
					}
				}
			}
		}
		c = tc
	}
	return area
}
func (b *Board) CountBonds() []int {
	bs := make([]int, len(b.Ps))
	for _, unit := range b.Units {
		adj := b.Adjacent(unit.Loc)
		for _, ad := range adj {
			if b.Map[ad] > 0 && b.Map[ad] != unit.P {
				bs[unit.P-1] += 1
			}
		}
	}
	return bs
}
func (b *Board) CountArea() []int {
	if len(b.Units) == 0 {
		return []int{0, 0, 0}
	}
	as := make([]int, len(b.Ps))
	seg := b.Segment()
	/*for _, g := range seg.Groups {
		as[g.P-1] += len(g.Locs)
	}*/
	for _, s := range seg.Spaces {
		adj := b.AdjacentLocs(s.Locs)
		p := 1
		mono := true
		for l, _ := range adj { //why?
			p = b.Map[l]
			break
		}
		for l, _ := range adj {
			if b.Map[l] != p {
				mono = false
				break
			}
		}
		if mono {
			as[p-1] += len(s.Locs)
		}
	}
	return as
}

type Group struct {
	Locs           map[Loc]struct{}
	P              int
	AdjacentGroups []Group
	AdjacentSpaces []Space
	Life           int
}
type Space struct {
	Locs           map[Loc]struct{}
	AdjacentGroups []Group
}
type Segmentation struct {
	Groups []*Group
	Spaces []*Space
	B      *Board
}

func (b *Board) AdjacentLocs(locs map[Loc]struct{}) map[Loc]struct{} {
	adj := make(map[Loc]struct{})
	for loc, _ := range locs {
		a := b.Adjacent(loc)
		for _, l := range a {
			_, ok := locs[l]
			_, ok2 := b.Map[l] //uneccesary?
			if !ok && ok2 {
				adj[l] = struct{}{}
			}
		}
	}
	return adj
}
func (b *Board) Segment() Segmentation {
	var seg Segmentation
	seg.B = b
	checked := make(map[Loc]struct{})
	for loc, p := range b.Map {
		_, ok := checked[loc]
		if !ok {
			a := b.GetArea(loc)
			if p == 0 {
				s := Space{a, []Group{}}
				seg.Spaces = append(seg.Spaces, &s)
			} else {
				g := Group{a, p, []Group{}, []Space{}, 0}
				seg.Groups = append(seg.Groups, &g)
			}
			for l, _ := range a {
				checked[l] = struct{}{}
			}
		}
	}
	return seg
}
func (b *Board) PlaceMap(m map[Loc]struct{}, p int) {
	for l, _ := range m {
		b.PlaceP(l, p)
	}
}
func SetLife(seg Segmentation) {
	for _, group := range seg.Groups {
		tb := Makeboard(seg.B.Layers)
		tb.PlaceMap(group.Locs, group.P)
		tseg := tb.Segment()
		if len(tseg.Spaces) > 1 {
			group.Life = 1
		} else {
			group.Life = 0
		}
	}
}
func (b *Board) Score() ([]int, []int, int, int, int) {
	seg := b.Segment()
	SetLife(seg)
	tboard := Makeboard(b.Layers)
	for _, g := range seg.Groups {
		if g.Life > 0 {
			tboard.PlaceMap(g.Locs, g.P)
		}
	}
	bs := tboard.CountBonds()
	as := tboard.CountArea()
	ta := 1
	nz := 0
	for _, a := range as {
		if a != 0 {
			ta = ta * a
		} else {
			nz += 1
		}
	}
	if nz == len(as) {
		ta = 0
	}
	tb := 1
	nz = 0
	for _, a := range bs {
		if a != 0 {
			tb = tb * a
		} else {
			nz += 1
		}
	}
	if nz == len(as) {
		tb = 0
	}
	t := ta * tb
	return as, bs, ta, tb, t
}
func (b *Board) Unitsstr() string {
	res := ""
	for _, unit := range b.Units {
		res += strconv.Itoa(unit.Loc.X) + "," + strconv.Itoa(unit.Loc.Y) + ":" + strconv.Itoa(unit.P) + ";"
	}
	return res
}
func LoadBoard(size int, units string) (*Board, error) {
	board := Makeboard(size)
	places := strings.Split(units, ";")
	var err error
	for _, place := range places {
		if place != "" {
			ta := strings.Split(place, ":")
			locs := strings.Split(ta[0], ",")
			x, err := strconv.Atoi(locs[0])
			if err != nil {
				err = errors.New("Unable to parse some numbers")
				continue
			}
			y, err := strconv.Atoi(locs[1])
			if err != nil {
				err = errors.New("Unable to parse some numbers")
				continue
			}
			p, err := strconv.Atoi(ta[1])
			if err != nil {
				err = errors.New("Unable to parse some numbers")
				continue
			}
			board.PlaceP(Loc{x, y}, p)
		}
	}
	return &board, err
}
